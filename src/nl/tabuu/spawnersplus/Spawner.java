/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import net.milkbowl.vault.economy.Economy;
import nl.tabuu.spawnersplus.utils.Skull;
import nl.tabuu.spawnersplus.utils.SkullDatabase;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;
import nl.tabuu.spawnersplus.utils.customconfig.CustomConfig;

public class Spawner implements InventoryHolder{

	private static HashMap<Location, Spawner> _spawners = new HashMap<Location, Spawner>();
	
	Plugin _plugin;
	Inventory _inventory;
	CustomConfig _settings, _data;
	FileConfiguration _language;
	ConfigManager _configManager;
	Block _block;
	List<ItemStack> _invItems = new ArrayList<ItemStack>();
	
	int _spawnerLevel = 0;
	int _maxLevel = 0;
	String _effect = null;
	Boolean _useVaultCurrency;
	Economy _economy;

	public Spawner(Block block) {
		_plugin = SpawnersPlus.getPlugin();
		_settings = ConfigManager.getConfig("settings");
		_data = ConfigManager.getConfig("data");
		_language = ConfigManager.getConfig("language").getData();
		_block = block;
		_maxLevel = _settings.getData().getInt("levels.MaxLevel");
		_useVaultCurrency = !_settings.getData().getBoolean("GeneralSettings.UpgradeWithXP");
		registerSpawner();
		
		_economy = SpawnersPlus.getEconomy();
		_spawners.put(_block.getLocation(), this);
	}

	public static Spawner get(Location location) {		
		return _spawners.get(location);
	}
	
	private void registerSpawner(){
		if(_data.getData().get("spawners." + locationToString(_block.getLocation()) + ".level") == null){
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".level", 0);
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".boost", 1.0d);
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".effect", _settings.getData().getString("levels." + 0 + ".effect"));
			_data.save();
		}
	}
	
	private String parseLanguageText(String path) {
		
		String spawnType = "";
		for(String string : ((CreatureSpawner) _block.getState()).getSpawnedType().name().split("_")) {
			string = string.toLowerCase();
			string = string.substring(0, 1).toUpperCase() + string.substring(1);
			spawnType += " " + string;
		}
		
		double nextCost = _settings.getData().getInt("levels." + (_spawnerLevel + 1) + ".cost");
		double nextBoost = _settings.getData().getInt("levels." + (_spawnerLevel + 1) + ".boost");
		double currentBoost = _data.getData().getDouble("spawners." + locationToString(_block.getLocation()) + ".boost");
		
		String out = _language.getString(path);
		out = out.replace("%l", _spawnerLevel + "");
		out = out.replace("%nextl", (_spawnerLevel + 1) + "");
		out = out.replace("%ml", _maxLevel + "");
		out = out.replace("%cb", currentBoost + "");
		out = out.replace("%c", nextCost + "");
		out = out.replace("%b", nextBoost + "");
		out = out.replace("%st", spawnType + "");
		
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
	
	private void createInventory(){
		_spawnerLevel = _data.getData().getInt("spawners." + locationToString(_block.getLocation()) + ".level");
		_inventory = Bukkit.getServer().createInventory(this, 27, ChatColor.BOLD + "" + ChatColor.GOLD + "Spawner Level " + _spawnerLevel);
		
		if(_spawnerLevel < _maxLevel && !_useVaultCurrency) {
			ItemStack upgrade = createItem(Material.EXP_BOTTLE, _spawnerLevel == 0 ? 1 : _spawnerLevel, parseLanguageText("GUI_UpgradeTo"), parseLanguageText("GUI_XPCost"), parseLanguageText("GUI_NextBoost"));		
			_inventory.setItem(15, upgrade);
		}
		else if(_spawnerLevel < _maxLevel && _useVaultCurrency) {
			ItemStack upgrade = createItem(Material.EXP_BOTTLE, _spawnerLevel == 0 ? 1 : _spawnerLevel, parseLanguageText("GUI_UpgradeTo"), parseLanguageText("GUI_VaultCost"), parseLanguageText("GUI_NextBoost"));		
			_inventory.setItem(15, upgrade);
		}
		else {
			ItemStack upgrade = createItem(Material.EXP_BOTTLE, _spawnerLevel == 0 ? 1 : _spawnerLevel, parseLanguageText("GUI_MaxLevel"));		
			_inventory.setItem(15, upgrade);
		}
		
		ItemStack info = createSkull(parseLanguageText("GUI_Info"), SkullDatabase.getData().get(((CreatureSpawner) _block.getState()).getSpawnedType()),parseLanguageText("GUI_CurrentLevel"), parseLanguageText("GUI_CurrentBoost"), parseLanguageText("GUI_SpawnType"));	
		_inventory.setItem(11, info);
		
		ItemStack close = createItem(Material.BARRIER, 1, parseLanguageText("GUI_Close"), "");		
		_inventory.setItem(22, close);
	}
	
	public void show(Player player) {
		createInventory();
		player.openInventory(_inventory);	
	}
	
	private ItemStack createSkull(String name, String url, String... description) {
		ItemStack stack = Skull.getCustomSkull(url);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + name);		
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		
		List<String> lore = new ArrayList<String>();
		for(String line : description) {
			lore.add(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + line);
		}
		
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	private ItemStack createItem(Material material, int amount, String name, String... description) {
		ItemStack stack = new ItemStack(material, amount);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + name);		
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		
		List<String> lore = new ArrayList<String>();
		for(String line : description) {
			lore.add(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + line);
		}
		
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		return stack;
	}

	public void upgrade(Player player) {
		if(_spawnerLevel >= _maxLevel) player.sendMessage(parseLanguageText("Error_SpawnerMaxLevel"));
		else if(!_useVaultCurrency && player.getLevel() < _settings.getData().getInt("levels." + (_spawnerLevel + 1) + ".cost")) player.sendMessage(parseLanguageText("Error_NotEnoughXP"));
		else if(_useVaultCurrency && _economy.getBalance(player) < _settings.getData().getDouble("levels." + (_spawnerLevel + 1) + ".cost")) player.sendMessage(parseLanguageText("Error_NotEnoughMoney"));
		else{
			boolean hasPermission = !_settings.getData().getBoolean("GeneralSettings.UpgradePermissions");
			for(int i = (_spawnerLevel + 1); i < _maxLevel; i++) {
				hasPermission = player.hasPermission("spawner.upgrade." + i) ? true : hasPermission;
			}
			if(!hasPermission) {
				player.sendMessage(parseLanguageText("Error_NoUpgradePermission"));
				return;
			}
			if (!_useVaultCurrency) player.setLevel(player.getLevel() - _settings.getData().getInt("levels." + (_spawnerLevel + 1) + ".cost"));
			else _economy.withdrawPlayer(player, _settings.getData().getDouble("levels." + (_spawnerLevel + 1) + ".cost"));
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".level", (_spawnerLevel + 1));
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".boost", _settings.getData().getDouble("levels." + (_spawnerLevel + 1) + ".boost"));
			_data.getData().set("spawners." + locationToString(_block.getLocation()) + ".effect", _settings.getData().getString("levels." + (_spawnerLevel + 1) + ".effect"));
			_data.save();
			player.sendMessage(parseLanguageText("Info_upgraded"));
			show(player);
		}
	}
	
	private String locationToString(Location location) {
		String world = location.getWorld().getName();

		int x = location.getBlockX(), y = location.getBlockY(), z = location.getBlockZ();

		return world + " " + x + " " + y + " " + z;
	}

	@Override
	public Inventory getInventory() {
		return _inventory;
	}
}
