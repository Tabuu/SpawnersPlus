/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;
import nl.tabuu.spawnersplus.commands.SpawnerCommand;
import nl.tabuu.spawnersplus.commands.SpawnerTabCompleteHandler;
import nl.tabuu.spawnersplus.events.PlayerEventHandler;
import nl.tabuu.spawnersplus.events.SpawnerHandler;
import nl.tabuu.spawnersplus.events.SpawnerHandler_1_8;
import nl.tabuu.spawnersplus.events.SpawnerRankupEvents;
import nl.tabuu.spawnersplus.events.SpawnerShopSignEvents;
import nl.tabuu.spawnersplus.utils.SkullDatabase;
import nl.tabuu.spawnersplus.utils.VersionChecker;
import nl.tabuu.spawnersplus.utils.bStats.Metrics;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;

public class SpawnersPlus extends JavaPlugin{

	private static Plugin _plugin;
	private static Economy _economy;
	
	private FileConfiguration _settings;
	
	@Override
	public void onEnable() {
		
		_plugin = this;
		
		String newestVersion = VersionChecker.getVersion();
		if(newestVersion == null) {
			Bukkit.getLogger().severe("[SpawnersPlus] Could not check for updates!");
		}
		else if(!this.getDescription().getVersion().equals(newestVersion)) {
			Bukkit.getLogger().info("[SpawnersPlus] New version avalible! (Version: " + newestVersion + ")" );
		}
		else {
			Bukkit.getLogger().info("[SpawnersPlus] Plugin up to date! (Version: " + newestVersion + ")" );
		}
		
		ConfigManager.setPlugin(_plugin);
		ConfigManager.addConfig("skulls");
		ConfigManager.addConfig("settings");
		ConfigManager.addConfig("data");
		ConfigManager.addConfig("language");
		_settings = ConfigManager.getConfig("settings").getData();
		
		if(_settings.getBoolean("GeneralSettings.UseVaultEconomy")) {
			registerVault();
		}
		
		registerEvents();
		new Metrics(this);
	}
	
	private void registerVault() {
		if(Bukkit.getPluginManager().getPlugin("Vault") instanceof Vault){
			RegisteredServiceProvider<Economy> service = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
			
			if (service != null) {
				_economy = service.getProvider();
				getServer().getPluginManager().registerEvents(new SpawnerShopSignEvents(), this);
			}
			else {
				Bukkit.getLogger().severe("Could not enable the Vault services. Download the latest version of Vault or change the UseVaultEconomy option in the settings.yml");
			}
		}
		else {
			Bukkit.getLogger().severe("You must have the Vault plugin installed! Download Vault or change the UseVaultEconomy option in the settings.yml");
		}
	}
	
	private void registerEvents() {
		
		this.getCommand("spawner").setExecutor(new SpawnerCommand());
		this.getCommand("spawner").setTabCompleter(new SpawnerTabCompleteHandler());
		
		getServer().getPluginManager().registerEvents(new PlayerEventHandler(), this);
		
		if(_settings.getBoolean("GeneralSettings.SpawnerRankupSystem")) {
			SkullDatabase.loadData();
			getServer().getPluginManager().registerEvents(new SpawnerRankupEvents(), this);
		}
		
		if(getBukkitVersion().equals("1.7") || getBukkitVersion().equals("1.8")) {
			getServer().getPluginManager().registerEvents(new SpawnerHandler_1_8(), this);
		}
		else {
			getServer().getPluginManager().registerEvents(new SpawnerHandler(), this);
		}
	}
	
	public static String getBukkitVersion() {
		if(Bukkit.getBukkitVersion().contains("1.7")) return "1.7";
		else if(Bukkit.getBukkitVersion().contains("1.8")) return "1.8";
		else if(Bukkit.getBukkitVersion().contains("1.9")) return "1.9";
		else if(Bukkit.getBukkitVersion().contains("1.10")) return "1.10";
		else if(Bukkit.getBukkitVersion().contains("1.11")) return "1.11";
		else if(Bukkit.getBukkitVersion().contains("1.12")) return "1.12";
		else return "";
	}
	
	public static Economy getEconomy() {
		return _economy;
	}
	
	public static Plugin getPlugin() {
		return _plugin;
	}
}
