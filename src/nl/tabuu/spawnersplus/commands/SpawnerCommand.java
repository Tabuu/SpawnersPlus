/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.commands;

import java.util.Arrays;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import nl.tabuu.spawnersplus.SpawnersPlus;
import nl.tabuu.spawnersplus.Spawner;
import nl.tabuu.spawnersplus.utils.VersionChecker;
import nl.tabuu.spawnersplus.utils.customconfig.*;

public class SpawnerCommand implements CommandExecutor {

	Plugin _plugin;
	CustomConfig _data, _settings;

	public SpawnerCommand() {
		_plugin = SpawnersPlus.getPlugin();

		_data = ConfigManager.getConfig("data");
		_settings = ConfigManager.getConfig("settings");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("spawner") && sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 0) {
				String newestVersion = VersionChecker.getVersion();
				String upToDateMessage = ChatColor.GRAY + "" + ChatColor.ITALIC + "(m)" + ChatColor.RESET;
				if(newestVersion == null) {
					upToDateMessage = upToDateMessage.replace("m", "Could not load newest version");
				}
				else {
					String message = !newestVersion.equals(_plugin.getDescription().getVersion()) ? "New version avalible! Version: " + newestVersion : "Up to date!";					
					upToDateMessage = upToDateMessage.replace("m", message);
				}
				
				String[] message = new String[] { " ",
						ChatColor.DARK_AQUA + "" + ChatColor.BOLD + _plugin.getDescription().getName() + " version "
								+ _plugin.getDescription().getVersion() + " by "
								+ _plugin.getDescription().getAuthors().get(0) + " " + upToDateMessage,

						ChatColor.GOLD + "/spawner upgrade",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Opens the GUI of the spawner.",

						ChatColor.GOLD + "/spawner setlevel <level>",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Sets the level of the spawner.",

						ChatColor.GOLD + "/spawner setmob <entity>",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Changes the spawntype of the spawner.",

						ChatColor.GOLD + "/spawner setparticle <particle> [amount]",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Changes the particle effect of the spawner.",

						ChatColor.GOLD + "/spawner give <player> <entity> [level]",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Changes the particle effect of the spawner.",

						ChatColor.GOLD + "/spawner reload",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Reloads all the .yml files", 
						
						ChatColor.GOLD + "/spawner cleandatabase",
						ChatColor.GRAY + "" + ChatColor.ITALIC + "Checks for each spawner if it still excists and if not remove its data."
				};

				player.sendMessage(message);
				return true;
			}

			boolean hasPermission;
			boolean optionDisabled;
			String correctUsage;
			switch (args[0]) {

			case "upgrade":
				hasPermission = player.hasPermission("spawner.gui");
				correctUsage = args.length != 1 ? "/spawner upgrade" : null;
				optionDisabled = !_settings.getData().getBoolean("GeneralSettings.SpawnerRankupSystem");
				break;

			case "setlevel":
				hasPermission = player.hasPermission("spawner.setlevel");
				correctUsage = args.length != 2 ? "/spawner setlevel <level>" : null;
				optionDisabled = !_settings.getData().getBoolean("GeneralSettings.SpawnerRankupSystem");
				break;

			case "setmob":
				hasPermission = player.hasPermission("spawner.setmob");
				correctUsage = args.length != 2 ? "/spawner setmob <entity>" : null;
				optionDisabled = false;
				break;

			case "setparticle":
				hasPermission = player.hasPermission("spawner.setparticle");
				correctUsage = args.length < 2 ? "/spawner setparticle <particle> [amount]" : null;
				optionDisabled = !_settings.getData().getBoolean("GeneralSettings.SpawnerRankupSystem");
				break;

			case "give":
				hasPermission = player.hasPermission("spawner.give");
				correctUsage = args.length < 3 ? "/spawner give <player> <entity> [level]" : null;
				optionDisabled = false;
				break;

			case "reload":
				hasPermission = player.hasPermission("spawner.reload");
				correctUsage = args.length != 1 ? "/spawner reload" : null;
				optionDisabled = false;
				break;
				
			case "cleandatabase":
				hasPermission = player.hasPermission("spawner.cleandatabase");
				correctUsage = args.length != 1 ? "/spawner cleandatabase" : null;
				optionDisabled = false;
				break;

			default:
				hasPermission = true;
				correctUsage = null;
				optionDisabled = false;
				break;
			}

			if (!hasPermission) {
				player.sendMessage(parseLanguageText("Error_NoPermission"));
				return true;
			} else if (correctUsage != null) {
				player.sendMessage(parseLanguageText("Error_WrongSynatax") + correctUsage);
				return true;
			} else if(optionDisabled) {
				player.sendMessage(parseLanguageText("Error_OptionDisabled"));
				return true;
			}

			Block targetBlock = player.getTargetBlock((Set<Material>) null, 15);
			CreatureSpawner spawner = null;
			if (targetBlock.getState() instanceof CreatureSpawner) {
				spawner = (CreatureSpawner) targetBlock.getState();
			}

			switch (args[0]) {

			case "upgrade":
				if (spawner != null) {
					Spawner gui = new Spawner(targetBlock);
					gui.show(player);
				} else {
					player.sendMessage(parseLanguageText("Error_NotLookingAtSpawner"));
				}
				break;

			case "setlevel":
				try {
					int level = Integer.parseInt(args[1]);
					double boost = _settings.getData().getDouble("levels." + level + ".boost");
					String effect = _settings.getData().getString("levels." + level + ".effect");
					
					setSpawnerData(targetBlock, "level", level);
					setSpawnerData(targetBlock, "boost", boost);
					setSpawnerData(targetBlock, "effect", effect);
				} catch (NumberFormatException exception) {
					player.sendMessage(ChatColor.RED + parseLanguageText("Error_NotANumber", args[1]));
				}
				break;

			case "setmob":
				if (spawner != null) {
					try {
						EntityType type = EntityType.valueOf(args[1].toUpperCase());
						spawner.setSpawnedType(type);
						spawner.update();
						player.sendMessage(parseLanguageText("Info_Setmob", type.name()));
					} catch (IllegalArgumentException ex) {
						player.sendMessage(parseLanguageText("Error_NotAnEntity", args[1]));
					}
				} else {
					player.sendMessage(parseLanguageText("Error_NotLookingAtSpawner"));
				}
				break;

			case "setparticle":
				if (spawner != null) {
					Particle particle = null;
					int particleAmount = 1;

					if(args.length == 3) {
						try {
							particleAmount = Integer.parseInt(args[2]);
						} catch (NumberFormatException exception) {
							player.sendMessage(parseLanguageText("Error_NotANumber", args[2]));
							break;
						}	
					}

					try {
						particle = Particle.valueOf(args[1]);
						String value = particle.name() + " " + particleAmount;
						setSpawnerData(targetBlock, "effect", value);
						player.sendMessage(parseLanguageText("Info_SetParticle", particle.name()));
					} catch (IllegalArgumentException e) {
						player.sendMessage(parseLanguageText("Error_NotAParticle", args[1]));
					}
				} else {
					player.sendMessage(parseLanguageText("Error_NotLookingAtSpawner"));
				}
				break;

			case "give":
				EntityType type = null;
				Player target = Bukkit.getPlayer(args[1]);

				if (target == null) {
					player.sendMessage(parseLanguageText("Error_PlayerNotFound", args[1]));
					break;
				}				

				try {
					type = EntityType.valueOf(args[2].toUpperCase());
				} catch (IllegalArgumentException ex) {
					player.sendMessage(parseLanguageText("Error_NotAnEntity", args[2]));
					break;
				}

				String itemName = "";
				for (String subString : type.name().split("_")) {
					subString = subString.substring(0, 1).toUpperCase() + subString.substring(1).toLowerCase();
					itemName += subString + " ";
				}

				ItemStack spawnerItem = new ItemStack(Material.MOB_SPAWNER, 1);
				ItemMeta meta = spawnerItem.getItemMeta();
				meta.setDisplayName(ChatColor.RESET + itemName + "Spawner");

				if (args.length == 4) {
					try {
						int level = Integer.parseInt(args[3]);
						meta.setLore(Arrays.asList(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + "Level " + level));
					} catch (NumberFormatException ex) {
						player.sendMessage(parseLanguageText("Error_NotANumber"));
						break;
					}
				}

				spawnerItem.setItemMeta(meta);
				target.getInventory().addItem(spawnerItem);
				break;

			case "reload":
				ConfigManager.reloadAll();
				player.sendMessage(ChatColor.GREEN + "SpawnersPlus reload complete.");
				break;
				
			case "cleandatabase":
				int removedSpawners = 0;
				Set<String> spawnerLocations = _data.getData().getConfigurationSection("spawners").getKeys(false);
				if(spawnerLocations == null) {
					player.sendMessage(ChatColor.RED + "No spawners found.");
					break;
				}
				for(String spawnerLocation : spawnerLocations) {
					Location location = stringToLocation(spawnerLocation);
					if(location.getBlock().getState() instanceof CreatureSpawner == false) {
						_data.getData().set("spawners." + spawnerLocation, null);
						removedSpawners++;
					}
				}
				_data.save();
				player.sendMessage(ChatColor.GREEN + "Removed " + removedSpawners + " spawner(s) from the database.");
				break;
				
			default:
				player.sendMessage(ChatColor.RED + "/spawner " + args[0] + " is not a valid command.");
				break;
			}
		}
		return true;
	}

	private String parseLanguageText(String path) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
	
	private String parseLanguageText(String path, String string) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = out.replace("%s", string);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
	
	private boolean setSpawnerData(Block block, String data, Object value) {
		if (block.getState() instanceof CreatureSpawner == false)
			return false;
		CreatureSpawner spawner = (CreatureSpawner) block.getState();
		String spawnerLocation = locationToString(spawner.getLocation());

		_data.getData().set("spawners." + spawnerLocation + "." + data, value);
		_data.save();

		return true;
	}

	private String locationToString(Location location) {
		String world = location.getWorld().getName();

		int x = location.getBlockX(), y = location.getBlockY(), z = location.getBlockZ();

		return world + " " + x + " " + y + " " + z;
	}
	
	private Location stringToLocation(String string) {
		String[] args = string.split(" ");

		World world = Bukkit.getWorld(args[0]);
		int x = Integer.parseInt(args[1]), y = Integer.parseInt(args[2]), z = Integer.parseInt(args[3]);

		float yaw = 0f, pitch = 0f;

		if (args.length > 4) {
			yaw = Float.parseFloat(args[4]);
			pitch = Float.parseFloat(args[5]);
		}

		return new Location(world, x, y, z, yaw, pitch);
	}
}
