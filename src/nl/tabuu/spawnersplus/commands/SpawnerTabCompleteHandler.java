/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.EntityType;

public class SpawnerTabCompleteHandler implements TabCompleter{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("spawner") && args[0].equalsIgnoreCase("setmob")) {
			if (args.length == 2) {
				ArrayList<String> entityTypes = new ArrayList<String>();
				if (!args[1].equals("")) {
					for (EntityType type : EntityType.values()) {
						if (type.isAlive() && type.name().toLowerCase().startsWith(args[1].toLowerCase())) {
							entityTypes.add(type.name());
						}
					}
				}
				else {
					for (EntityType type : EntityType.values()) {
						if (type.isAlive()) {
							entityTypes.add(type.name());
						}
					}
				}
				Collections.sort(entityTypes);

				return entityTypes;
			}
		}
		
		if (cmd.getName().equalsIgnoreCase("spawner") && args[0].equalsIgnoreCase("setparticle")) {
			if (args.length == 2) {
				ArrayList<String> particleTypes = new ArrayList<String>();
				if (!args[1].equals("")) {
					try {
						for (Particle type : Particle.values()) {
							if (type.name().toLowerCase().startsWith(args[1].toLowerCase())) {
								particleTypes.add(type.name());
							}
						}
					}catch(NoClassDefFoundError ex) {
						
					}				
				}
				else {
					try {
						for (Particle type : Particle.values()) {
							particleTypes.add(type.name());
						}
					}catch(NoClassDefFoundError ex) {
						
					}
				}
				Collections.sort(particleTypes);

				return particleTypes;
			}
		}
		
		if (cmd.getName().equalsIgnoreCase("spawner") && args[0].equalsIgnoreCase("give")) {
			if (args.length == 3) {
				ArrayList<String> entityTypes = new ArrayList<String>();
				if (!args[2].equals("")) {
					for (EntityType type : EntityType.values()) {
						if (type.isAlive() && type.name().toLowerCase().startsWith(args[2].toLowerCase())) {
							entityTypes.add(type.name());
						}
					}
				}
				else {
					for (EntityType type : EntityType.values()) {
						if (type.isAlive()) {
							entityTypes.add(type.name());
						}
					}
				}
				Collections.sort(entityTypes);

				return entityTypes;
			}
		}
		return null;
	}
}
