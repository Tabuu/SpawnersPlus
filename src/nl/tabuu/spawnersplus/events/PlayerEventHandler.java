/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.events;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SpawnEggMeta;
import org.bukkit.plugin.Plugin;

import nl.tabuu.spawnersplus.SpawnersPlus;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;

public class PlayerEventHandler implements Listener {

	Plugin _plugin;
	FileConfiguration _data, _config;

	public PlayerEventHandler() {
		_plugin = SpawnersPlus.getPlugin();

		_data = ConfigManager.getConfig("data").getData();
		_config = ConfigManager.getConfig("settings").getData();
	}

	@EventHandler
	public void onPlayerUseSpawnEgg(PlayerInteractEvent event) {

		Block block = event.getClickedBlock();
		ItemStack item = event.getItem();
		
		if (block != null && item != null && block.getType().equals(Material.MOB_SPAWNER)
				&& item.getType().equals(Material.MONSTER_EGG)
				&& (!_config.getBoolean("GeneralSettings.AllowEggChangeSpawner")
						|| !event.getPlayer().getGameMode().equals(GameMode.CREATIVE))) {
			event.setCancelled(true);
			SpawnEggMeta meta = (SpawnEggMeta) item.getItemMeta();
			
			Location location = block.getLocation();

			location.setX(location.getX() + event.getBlockFace().getModX());
			location.setY(location.getY() + event.getBlockFace().getModY());
			location.setZ(location.getZ() + event.getBlockFace().getModZ());

			Block spawnBlock = location.getBlock();

			Location spawnLocation = spawnBlock.getLocation();
			spawnLocation.setX(spawnLocation.getX() + 0.5d);
			spawnLocation.setZ(spawnLocation.getZ() + 0.5d);

			block.getWorld().spawnEntity(spawnLocation, meta.getSpawnedType());
			item.setAmount(item.getAmount() - 1);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (e.getInventory() instanceof AnvilInventory && e.getSlotType().equals(SlotType.RESULT)
				&& e.getCurrentItem().getType().equals(Material.MOB_SPAWNER)) {
			e.setCancelled(true);
			e.getWhoClicked().sendMessage(ChatColor.RED + parseLanguageText("Error_Renaming"));
		}
	}
	
	private String parseLanguageText(String path) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
}
