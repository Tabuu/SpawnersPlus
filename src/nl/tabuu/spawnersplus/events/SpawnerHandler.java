/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import nl.tabuu.spawnersplus.SpawnersPlus;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;
import nl.tabuu.spawnersplus.utils.customconfig.CustomConfig;
import nl.tabuu.spawnersplus.Spawner;

public class SpawnerHandler implements Listener{

	Plugin _plugin;
	CustomConfig _data, _settings;
	
	public SpawnerHandler() {
		_plugin = SpawnersPlus.getPlugin();	
		_data = ConfigManager.getConfig("data");
		_settings = ConfigManager.getConfig("settings");
	}
	
	
	@EventHandler
	public void onSpawnerExplode(EntityExplodeEvent e) {
		for(Block block : e.blockList()) {
			if(block.getType().equals(Material.MOB_SPAWNER)) {
				Random random = new Random();
				if(_settings.getData().getDouble("GeneralSettings.ExplosionDropChance") < random.nextDouble()) continue;
				CreatureSpawner spawner = (CreatureSpawner) block.getState();
				String name = "";
				for(String subString :  spawner.getSpawnedType().name().split("_")) {
					subString = subString.substring(0, 1).toUpperCase() + subString.substring(1).toLowerCase();
					name += subString + " ";
				}
				int level = _data.getData().getInt("spawners." + locationToString(block.getLocation()) + ".level");
				block.getWorld().dropItemNaturally(block.getLocation(), createItem(Material.MOB_SPAWNER, 1, name + "Spawner", "Level " + level));
			}
		}
	}
	
	@EventHandler
	public void onSpawnerBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		Player player = e.getPlayer();

		int level = 0;
		if (_data.getData().getConfigurationSection("spawners." + locationToString(block.getLocation())) != null) {
			level = _data.getData().getInt("spawners." + locationToString(block.getLocation()) + ".level");
			_data.getData().set("spawners." + locationToString(block.getLocation()), null);
			_data.save();
		}
		
		if(player.hasPermission("spawner.mine") && !player.getGameMode().equals(GameMode.CREATIVE) && e.getPlayer().getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)) {
			if (block.getState() instanceof CreatureSpawner) {
				CreatureSpawner spawner = (CreatureSpawner) block.getState();
				e.setExpToDrop(0);
				String name = "";
				for(String subString :  spawner.getSpawnedType().name().split("_")) {
					subString = subString.substring(0, 1).toUpperCase() + subString.substring(1).toLowerCase();
					name += subString + " ";
				}
				block.getWorld().dropItemNaturally(e.getBlock().getLocation(), createItem(Material.MOB_SPAWNER, 1, name + "Spawner", "Level " + level));
			}
		}
	}
	
	@EventHandler
	public void onSpawnerPlace(BlockPlaceEvent e) {
		ItemStack item = e.getItemInHand();
		CreatureSpawner spawner = null;
		Player player = e.getPlayer();
		if(e.getBlockPlaced().getType().equals(Material.MOB_SPAWNER)) {
			if(!player.hasPermission("spawner.place")) {
				e.setCancelled(true);
				player.sendMessage(parseLanguageText("Error_PlacingSpawner"));
				return;
			}
			spawner = (CreatureSpawner) e.getBlockPlaced().getState();
			try{
				String eType = ChatColor.stripColor(item.getItemMeta().getDisplayName());
				eType = eType.replaceAll(" Spawner", "");
				eType = eType.replace(' ', '_');
				eType = eType.toUpperCase();
				EntityType entity = EntityType.valueOf(eType);
				spawner.setSpawnedType(entity);
				spawner.update();
				
				new Spawner(e.getBlock());
				ItemMeta meta = e.getItemInHand().getItemMeta();
				int level = 0;
				if(meta.getLore().get(0).contains("Level")) {
					level = Integer.parseInt(meta.getLore().get(0).split(" ")[1]);
				}
				_data.getData().set("spawners." + locationToString(e.getBlock().getLocation()) + ".level", level);
				_data.getData().set("spawners." + locationToString(e.getBlock().getLocation()) + ".boost", _settings.getData().getDouble("levels." + level + ".boost"));
				_data.getData().set("spawners." + locationToString(e.getBlock().getLocation()) + ".effect", _settings.getData().getString("levels." + level + ".effect"));
				_data.save();
			}catch(IllegalArgumentException | NullPointerException exeption) {
				
			}		
		}
	}
	
	private String parseLanguageText(String path) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
	
	private ItemStack createItem(Material material, int amount, String name, String... description) {
		ItemStack stack = new ItemStack(material, amount);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + name);		
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		
		List<String> lore = new ArrayList<String>();
		for(String line : description) {
			lore.add(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + line);
		}
		
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		return stack;
	}
	
	private String locationToString(Location location) {
		String world = location.getWorld().getName();

		int x = location.getBlockX(), y = location.getBlockY(), z = location.getBlockZ();

		return world + " " + x + " " + y + " " + z;
	}
}
