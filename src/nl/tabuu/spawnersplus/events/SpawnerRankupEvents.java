/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.events;

import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

import nl.tabuu.spawnersplus.Spawner;
import nl.tabuu.spawnersplus.SpawnersPlus;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;

public class SpawnerRankupEvents implements Listener{
	
	Plugin _plugin;
	FileConfiguration _data, _settings;
	
	public SpawnerRankupEvents() {
		_plugin = SpawnersPlus.getPlugin();	
		_data = ConfigManager.getConfig("data").getData();
		_settings = ConfigManager.getConfig("settings").getData();
	}
	
	@EventHandler
	public void onPlayerSpawnerClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && !e.getPlayer().isSneaking()) {
			if (e.getClickedBlock().getState() instanceof CreatureSpawner && e.getPlayer().hasPermission("spawner.gui")) {
				Spawner gui = new Spawner(e.getClickedBlock());
				gui.show(e.getPlayer());
			}
		}
	}
	
	@EventHandler
	public void onSpawnerSpawn(SpawnerSpawnEvent e) {
		CreatureSpawner spawner = e.getSpawner();
		if(_data.getConfigurationSection("spawners." + locationToString(spawner.getLocation())) == null) return;
		int spawnerLevel = _data.getInt("spawners." + locationToString(spawner.getBlock().getLocation()) + ".level");
		if (spawnerLevel > 0) {
			Random random = new Random();
			int delay = random.nextInt((799 - 200) + 1) + 200;
			double boost = _data.getDouble("spawners." + locationToString(spawner.getLocation()) + ".boost");
			
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(_plugin, new Runnable() {
				public void run() {
					spawner.setDelay((int) Math.round(delay / boost <= 0 ? 1 : boost));
				}
			}, 5L);
		}

		Location location = spawner.getLocation();
		location.setX(location.getX() + 0.5);
		location.setY(location.getY() + 2);
		location.setZ(location.getZ() + 0.5);

		String unfParticle = _data.getString("spawners." + locationToString(spawner.getLocation()) + ".effect");
		
		Particle particle = null;
		try {
			int count = Integer.parseInt(unfParticle.toUpperCase().split(" ")[1]);
			particle = Particle.valueOf(unfParticle.toUpperCase().split(" ")[0]);
			spawner.getWorld().spawnParticle(particle, location, count, 0d, 0d, 0d);
		}
		catch(NoClassDefFoundError exeption) {
			Bukkit.getLogger().severe("The particle " + unfParticle.toUpperCase().split(" ")[0] + " is not supported for your Bukkit/Spigot version.");
		}
		catch(NullPointerException | NumberFormatException exeption) {
			
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		
		if (!e.getInventory().getName().startsWith(ChatColor.BOLD + "" + ChatColor.GOLD + "Spawner Level "))
			return;
		if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null)
			return;
		e.setCancelled(true);
		
		switch(e.getCurrentItem().getType()){
			case EXP_BOTTLE:
				Spawner.get(player.getTargetBlock((Set<Material>) null, 15).getLocation()).upgrade(player);
				break;
				
			case BARRIER:
				player.closeInventory();
				break;
				
			default:
				break;
		}
	}
	
	private String locationToString(Location location) {
		String world = location.getWorld().getName();

		int x = location.getBlockX(), y = location.getBlockY(), z = location.getBlockZ();

		return world + " " + x + " " + y + " " + z;
	}
}
