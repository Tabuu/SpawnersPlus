/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.events;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.milkbowl.vault.economy.Economy;
import nl.tabuu.spawnersplus.SpawnersPlus;
import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;

public class SpawnerShopSignEvents implements Listener{

	Economy _economy;
	
	public SpawnerShopSignEvents() {
		_economy = SpawnersPlus.getEconomy();
	}
	
	@EventHandler
	public void onSignClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getState() instanceof Sign) {
				Sign sign = (Sign) e.getClickedBlock().getState();
				Player player = e.getPlayer();
				if(!player.hasPermission("spawner.signshop.use")) return;
				if(!sign.getLine(0).equals(ChatColor.DARK_BLUE + "[SpawnerShop]")) return;
				double price = Double.parseDouble(sign.getLine(3).substring(1));
				EntityType eType = EntityType.valueOf(sign.getLine(2));
				
				if(sign.getLine(1).equalsIgnoreCase("buy")) {
					if(_economy.has(player, price)) {
						_economy.withdrawPlayer(player, price);
						player.getInventory().addItem(getSpawner(eType));
						player.sendMessage(parseLanguageText("Info_BoughtItem"));
					}
					else {
						player.sendMessage(parseLanguageText("Error_NotEnoughMoney"));
					}
				}
				else if(sign.getLine(1).equalsIgnoreCase("sell")) {
					for(ItemStack item : player.getInventory().getContents()) {
						if(item == null || item.getType() == null || item.getType() == Material.AIR || !item.hasItemMeta()) continue;
						if(item.getItemMeta().getDisplayName().equals(getSpawnerName(eType)) && item.getType() == Material.MOB_SPAWNER && item.getItemMeta().getLore().get(0).contains("Level")) {
							item.setAmount(item.getAmount() - 1);
							_economy.depositPlayer(player, price);
							player.sendMessage(parseLanguageText("Info_SoldItem"));
							return;
						}
					}
					player.sendMessage(parseLanguageText("Error_MissingRequiredItem"));
				}		
			}
		}
	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		Player player = e.getPlayer();
		if(!player.hasPermission("spawner.signshop.create")) return;
		if(e.getLine(0).equalsIgnoreCase("[spawnershop]")) {
			try{
				EntityType.valueOf(e.getLine(2));
			}catch(IllegalArgumentException exeption) {
				e.setLine(0, ChatColor.DARK_BLUE + "[SpawnerShop]");
				e.setLine(2, ChatColor.RED + e.getLine(2));
				player.sendMessage(parseLanguageText("Error_NotAnEntity", e.getLine(2)));
				return;
			}
			
			try{
				Double.parseDouble(e.getLine(3));
			}catch(NumberFormatException exeption) {
				e.setLine(0, ChatColor.DARK_BLUE + "[SpawnerShop]");
				e.setLine(2, ChatColor.RED + e.getLine(3));
				player.sendMessage(parseLanguageText("Error_NotANumber", e.getLine(3)));
				return;
			}
			
			if(!e.getLine(1).equalsIgnoreCase("sell") && !e.getLine(1).equalsIgnoreCase("buy")) {
				e.setLine(0, ChatColor.DARK_BLUE + "[SpawnerShop]");
				e.setLine(1, ChatColor.RED + e.getLine(1));
				player.sendMessage(ChatColor.RED + e.getLine(1) + " is not a valid argument. Use sell or buy.");
				return;
			}
			else {
				e.setLine(0, ChatColor.DARK_BLUE + "[SpawnerShop]");
				e.setLine(3, "$" + e.getLine(3));
				player.sendMessage(ChatColor.GREEN + "SpawnerShop sign successfully created.");
			}
		}
	}
	
	private ItemStack getSpawner(EntityType type) {
		String itemName = "";
		
		String[] args = type.name().split("_");
		for(String string : args) {
			itemName += string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase() + " ";
		}
		
		itemName += "Spawner";
		
		ItemStack spawner = new ItemStack(Material.MOB_SPAWNER, 1);
		ItemMeta itemMeta = spawner.getItemMeta();
		
		itemMeta.setDisplayName(ChatColor.RESET + itemName);
		itemMeta.setLore(Arrays.asList(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + "Level 0"));
		
		spawner.setItemMeta(itemMeta);
		return spawner;
	}
	
	private String getSpawnerName(EntityType type) {
		String itemName = "";
		
		String[] args = type.name().split("_");
		for(String string : args) {
			itemName += string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase() + " ";
		}
		
		itemName += "Spawner";
		
		return ChatColor.RESET + itemName;
	}
	
	private String parseLanguageText(String path) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
	
	private String parseLanguageText(String path, String string) {
		String out = ConfigManager.getConfig("language").getData().getString(path);
		out = out.replace("%s", string);
		out = ChatColor.translateAlternateColorCodes('&', out);
		
		return out;
	}
}
