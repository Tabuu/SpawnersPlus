/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.utils;

import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;

import nl.tabuu.spawnersplus.utils.customconfig.ConfigManager;

public class SkullDatabase {

	private static HashMap<EntityType, String> _skullDB = new HashMap<EntityType, String>();
	
	public static void loadData() {
		FileConfiguration data = ConfigManager.getConfig("skulls").getData();
		
		for(String keyString : data.getConfigurationSection("").getKeys(false)) {
			EntityType key = EntityType.valueOf(keyString);
			String value = data.getString(keyString);
			_skullDB.put(key, value);
		}
	}
	
	public static HashMap<EntityType, String> getData() {
		return _skullDB;
	}
}
