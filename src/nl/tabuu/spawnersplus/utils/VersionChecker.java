/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author Tabuu
 */

public class VersionChecker {

	
	private VersionChecker() {
		
	}
	
	public static String getVersion() {
		String version = null;
		
		try {
			URL u = new URL("https://www.spigotmc.org/resources/spawnersplus.45139/history");
	        HttpsURLConnection http = (HttpsURLConnection)u.openConnection();
	        http.setAllowUserInteraction(true);
	        http.addRequestProperty("User-Agent", "Mozilla/4.76");
	        http.connect();

	        InputStream is = http.getInputStream();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	        String line;
	        while ((line = reader.readLine()) != null)
	        {
				if(line.contains("<td class=\"version\">")) {
					line = line.replace("<td class=\"version\">", "");
					line = line.replace("</td>", "");
					version = line;
					reader.close();
					return version;
				}
	        }
		}catch(Exception ex) {
			version = null;
		}
        return version;
	}
}
