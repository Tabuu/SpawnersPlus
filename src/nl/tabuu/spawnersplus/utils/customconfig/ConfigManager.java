/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.utils.customconfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;

import org.bukkit.plugin.Plugin;

public class ConfigManager {
	private static Plugin _plugin = null;

	static HashMap<String, CustomConfig> _configs= new HashMap<String, CustomConfig>();
	
	private ConfigManager() {
		
	}
	
	public static void setPlugin(Plugin plugin) {
		_plugin = plugin;
	}
	
	public static void addConfig(String name) {
		_configs.put(name ,new CustomConfig(name + ".yml", "",_plugin.getResource(name + ".yml"), _plugin));
	}
	
	public static void addConfig(String name, String path) {
		_configs.put(name ,new CustomConfig(name + ".yml", path,_plugin.getResource(name + ".yml"), _plugin));
	}
	
	public static CustomConfig getConfig(String name) {
		return _configs.get(name);
	}
	
	public static void reloadAll() {
		for(CustomConfig config : _configs.values()) {
			config.reload();
		}
	}
	
	public static void loadConfigFolder(String path) {
		File folder = new File(path);
		
		for(File file : folder.listFiles()) {
			if(file.getName().endsWith(".yml")) {
				FileInputStream inputStream;
				try {
					inputStream = new FileInputStream(file);
					String name = file.getName().replace(".yml", "");
					_configs.put(name ,new CustomConfig(file.getName(), "", inputStream, _plugin));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
