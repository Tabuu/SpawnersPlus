/** Copyright 2017 Rick van Sloten
	All rights reserved.
*/

package nl.tabuu.spawnersplus.utils.customconfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class CustomConfig {

	File _file;
	File _path;
	FileConfiguration _config;
	Plugin _plugin;

	InputStream _defaults = null;

	public CustomConfig(String name, String path,InputStream defaults, Plugin plugin) {
		_plugin = plugin;
		_defaults = defaults;
		_file = new File(_plugin.getDataFolder() + path, name);
		_path = new File(_plugin.getDataFolder() + path);
		_config = new YamlConfiguration();

		createFile();
	}

	public void save() {	
		try {
			String str = _config.saveToString();

			FileWriter fw = new FileWriter(_file);
			fw.write(str);
			fw.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void createFile() {
		if (!_path.exists()) {
			_path.mkdirs();
		}
		if (_defaults != null) {
			try {
				if (!_file.exists()) {
					InputStream in = _defaults;
					OutputStream out = new FileOutputStream(_file);
					byte[] buf = new byte[_defaults.available()];
					int len;
					while ((len = in.read(buf)) > 0)
						out.write(buf, 0, len);
					out.close();
					in.close();
				}
				_config = YamlConfiguration.loadConfiguration(_file);
			} catch (IOException ex) {
				_plugin.getLogger().severe("Plugin unable to write configuration file " + _file.getName() + "!");
				_plugin.getLogger().severe("Disabling...");
				_plugin.getServer().getPluginManager().disablePlugin(_plugin);
				ex.printStackTrace();
			}
		}
	}

	public void reload() {
		_config = YamlConfiguration.loadConfiguration(_file);
	}

	public FileConfiguration getData() {
		return _config;
	}
}
